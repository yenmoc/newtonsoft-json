# Newtonsoft Json

## What

* Newtonsoft.Json v12.0.3 .Net 4.5

## Installation

```bash
"com.yenmoc.newtonsoft-json":"https://gitlab.com/yenmoc/newtonsoft-json"
or
npm publish --registry http://localhost:4873
```

